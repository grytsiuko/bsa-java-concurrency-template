package bsa.java.concurrency.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
public class ExecutorServiceConfiguration {

    @Bean(name = "fileStorageThreadPool")
    public ExecutorService fixedThreadPool() {
        int THREAD_POOL_SIZE = 4;
        return Executors.newFixedThreadPool(THREAD_POOL_SIZE);
    }
}
