package bsa.java.concurrency.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MultipartException;

@ControllerAdvice
public class ExceptionsHandler {

    @ExceptionHandler({MultipartException.class})
    public ResponseEntity<Object> handleTooLargeFile() {
        return new ResponseEntity<>(
                "Too large file sent",
                HttpStatus.PAYLOAD_TOO_LARGE
        );
    }
}
