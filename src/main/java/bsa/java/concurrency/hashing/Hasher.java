package bsa.java.concurrency.hashing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

@Component
public class Hasher {

    private static final Logger log = LoggerFactory.getLogger(Hasher.class);

    private final int WIDTH = 9;
    private final int HEIGHT = 9;

    public long calculateHash(MultipartFile file) {
        log.info("Starting calculating hash");
        BufferedImage image = prepareImage(file);
        long result = computeGradient(image);

        log.info("Ended calculating hash  ");
        return result;
    }

    private long computeGradient(BufferedImage image) {
        long hash = 0;
        for (int i = 1; i < HEIGHT; i++) {
            for (int k = 1; k < WIDTH; k++) {
                int curr = image.getRGB(i, k);
                int prev = image.getRGB(i - 1, k - 1);
                hash |= curr > prev ? 1 : 0;
                hash <<= 1;
            }
        }
        return hash;
    }

    private BufferedImage prepareImage(MultipartFile file) {
        Image scaledImage = getScaledImage(file);
        return grayScaleImage(scaledImage);
    }

    private Image getScaledImage(MultipartFile file) {
        try {
            Image initImage = ImageIO.read(file.getInputStream());
            return initImage.getScaledInstance(WIDTH, HEIGHT, Image.SCALE_SMOOTH);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        } catch (NullPointerException e) {
            throw new RuntimeException("Uploaded file is not image");
        }
    }

    private BufferedImage grayScaleImage(Image image) {
        BufferedImage grayScaleImage = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_BYTE_GRAY);
        grayScaleImage.getGraphics().drawImage(image, 0, 0, null);

        return grayScaleImage;
    }
}
