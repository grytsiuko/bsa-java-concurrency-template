package bsa.java.concurrency.image;

import bsa.java.concurrency.image.dto.SearchResultDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/image")
public class ImageController {

    private static final Logger log = LoggerFactory.getLogger(ImageService.class);

    private final ImageService imageService;

    @Autowired
    public ImageController(ImageService imageService) {
        this.imageService = imageService;
    }

    // Some logging seems to be redundant
    // However, they could prove that all tasks done and concurrency works correctly
    // For example, /batch waits for all images to be uploaded, while /search does not
    // Or saving to DB runs only after calculating hash and saving to hard drive

    @PostMapping("/batch")
    @ResponseStatus(HttpStatus.CREATED)
    public CompletableFuture<Void> batchUploadImages(@RequestParam("images") MultipartFile[] files) {
        log.info("### Starting uploading images");
        return imageService
                .uploadAllImages(files)
                .thenAccept(v -> log.info("### Sending response about uploading images"));
    }

    @PostMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    public List<SearchResultDTO> searchMatches(@RequestParam("image") MultipartFile file,
                                               @RequestParam(value = "threshold", defaultValue = "0.9") double threshold) {
        log.info("### Starting searching similar images");
        var result = imageService.searchSimilar(file, threshold);

        log.info("### Sending response about searching");
        return result;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public CompletableFuture<Void> deleteImage(@PathVariable("id") UUID imageId) {
        log.info("### Starting deletion image");
        return imageService.deleteOne(imageId)
                .thenAccept(v -> log.info("### Sending response about deletion image"));
    }

    @DeleteMapping("/purge")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public CompletableFuture<Void> purgeImages() {
        log.info("### Starting deletion all images");
        return imageService.deleteAll()
                .thenAccept(v -> log.info("### Sending response about deletion all images"));
    }
}
