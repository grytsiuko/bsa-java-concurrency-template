package bsa.java.concurrency.image;

import bsa.java.concurrency.image.dto.SearchResultDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Repository
public interface ImageRepository extends JpaRepository<ImageEntity, UUID> {

    @Query(nativeQuery = true,
            value = "SELECT result.imageId, " +
                    "       result.match * 100 AS matchPercent, " +
                    "       result.imageUrl " +
                    "FROM (SELECT cast(id as varchar) AS imageId, " +
                    "             url AS imageUrl, " +
                    "             1 - ( " +
                    "                  SELECT SUM((cast(hash # :hash AS bigint) >> bit) & 1) " +
                    "                  FROM generate_series(0, 63) bit " +
                    "             ) / 64 AS match " +
                    "      FROM images) result " +
                    "WHERE result.match >= :threshold " +
                    "ORDER BY result.match DESC")
    List<SearchResultDTO> findSimilarByHash(Long hash, Double threshold);

    @Transactional
    @Modifying
    @Query("DELETE " +
            "FROM ImageEntity")
    void deleteAll();
}
