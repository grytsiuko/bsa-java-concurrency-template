package bsa.java.concurrency.image;

import bsa.java.concurrency.hashing.Hasher;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.storage.Storage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Service
public class ImageService {

    private static final Logger log = LoggerFactory.getLogger(ImageService.class);

    private final Hasher hasher;

    private final Storage storage;

    private final ImageRepository imageRepository;

    @Autowired
    public ImageService(Hasher hasher, Storage storage, ImageRepository imageRepository) {
        this.hasher = hasher;
        this.storage = storage;
        this.imageRepository = imageRepository;
    }

    public CompletableFuture<Void> uploadAllImages(MultipartFile[] files) {
        var futures = Arrays.stream(files)
                .parallel()
                .map(this::uploadImage)
                .toArray(CompletableFuture[]::new);
        return CompletableFuture.allOf(futures);
    }

    public List<SearchResultDTO> searchSimilar(MultipartFile file, double threshold) {
        var hash = hasher.calculateHash(file);
        var similar = imageRepository.findSimilarByHash(hash, threshold);

        if (similar.isEmpty()) {
            CompletableFuture.runAsync(() -> uploadImage(file, hash));
        }
        return similar;
    }

    private CompletableFuture<Void> uploadImage(MultipartFile file) {
        return uploadImage(file, null);
    }

    private CompletableFuture<Void> uploadImage(MultipartFile file, Long providedHash) {
        UUID uuid = UUID.randomUUID();
        log.info("Starting uploading image  " + uuid);

        return save(file, uuid, providedHash)
                .thenAccept(v -> log.info("Ended uploading image     " + uuid));
    }

    private CompletableFuture<Void> save(MultipartFile file, UUID uuid, Long providedHash) {
        var urlFuture = storage.saveFile(file, uuid.toString());
        long hash = (providedHash == null)
                ? hasher.calculateHash(file)
                : providedHash;

        return urlFuture.thenAccept(url -> saveToDatabase(uuid, hash, url));
    }

    private void saveToDatabase(UUID uuid, long hash, String url) {
        log.info("Starting saving to DB ");
        var imageEntity = new ImageEntity(uuid, hash, url);
        imageRepository.save(imageEntity);
        log.info("Ended saving to DB   ");
    }

    public CompletableFuture<Void> deleteOne(UUID uuid) {
        return imageRepository
                .findById(uuid)
                .map(this::deletePresent)
                .orElse(CompletableFuture.allOf());
    }

    private CompletableFuture<Void> deletePresent(ImageEntity imageEntity) {
        var storageFuture = storage.deleteByUrl(imageEntity.getUrl());
        var dbFuture = CompletableFuture.runAsync(() -> deletePresentFromDatabase(imageEntity));
        return CompletableFuture.allOf(storageFuture, dbFuture);
    }

    public CompletableFuture<Void> deleteAll() {
        var storageFuture = storage.deleteAll();
        var dbFuture = CompletableFuture.runAsync(this::deleteAllFromDatabase);
        return CompletableFuture.allOf(storageFuture, dbFuture);
    }

    private void deletePresentFromDatabase(ImageEntity imageEntity) {
        log.info("Starting image deletion from DB ");
        imageRepository.delete(imageEntity);
        log.info("Ended image deletion from DB ");
    }

    private void deleteAllFromDatabase() {
        log.info("Starting all images deletion from DB ");
        imageRepository.deleteAll();
        log.info("Ended all images deletion from DB ");
    }
}
