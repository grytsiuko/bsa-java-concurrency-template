package bsa.java.concurrency.storage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

@Component
public class FileSystemStorage implements Storage {

    private static final Logger log = LoggerFactory.getLogger(FileSystemStorage.class);

    @Value(value = "${static_prefix}")
    private String STATIC_PREFIX;

    @Value(value = "${images_directory}")
    private String IMAGES_DIRECTORY;

    private final ExecutorService executorService;

    @Autowired
    public FileSystemStorage(@Qualifier("fileStorageThreadPool") ExecutorService executorService) {
        this.executorService = executorService;
    }

    public CompletableFuture<String> saveFile(MultipartFile file, String id) {
        return CompletableFuture.supplyAsync(
                () -> saveFileTask(file, id),
                executorService
        );
    }

    private String saveFileTask(MultipartFile file, String id) {
        log.info("Starting saving to file storage");
        String fileName = generateName(id, file);
        writeFile(generatePath(fileName), file);

        log.info("Ended saving to file storage");
        return generateUrl(fileName);
    }

    private void writeFile(Path path, MultipartFile file) {
        try (OutputStream stream = Files.newOutputStream(path)) {
            stream.write(file.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public CompletableFuture<Void> deleteByUrl(String url) {
        return CompletableFuture.runAsync(
                () -> deleteByUrlTask(url),
                executorService
        );
    }

    public CompletableFuture<Void> deleteAll() {
        return CompletableFuture.runAsync(
                this::deleteAllTask,
                executorService
        );
    }

    public void deleteByUrlTask(String url) {
        log.info("Starting deletion file from storage");
        String fileName = getFileName(url);
        File file = new File(IMAGES_DIRECTORY, fileName);

        file.delete();
        log.info("Ended deletion file from storage");
    }

    public void deleteAllTask() {
        log.info("Starting deletion all files from storage");
        var files = new File(IMAGES_DIRECTORY).listFiles();

        if (files != null) {
            Arrays.stream(files)
                    .forEach(File::delete);
        }
        log.info("Ended deletion all files from storage");
    }

    private Path generatePath(String name) {
        new File(IMAGES_DIRECTORY).mkdirs();
        return Path.of(IMAGES_DIRECTORY, name);
    }

    private String generateName(String id, MultipartFile file) {
        String originalName = file.getOriginalFilename();
        String extension = (originalName != null)
                ? originalName.substring(originalName.lastIndexOf('.'))
                : ".png";
        return id + extension;
    }

    private String generateUrl(String fileName) {
        return STATIC_PREFIX + fileName;
    }

    private String getFileName(String url) {
        return url.substring(STATIC_PREFIX.length());
    }
}
