package bsa.java.concurrency.storage;

import org.springframework.web.multipart.MultipartFile;

import java.util.concurrent.CompletableFuture;

public interface Storage {

    CompletableFuture<String> saveFile(MultipartFile file, String id);

    CompletableFuture<Void> deleteByUrl(String url);

    CompletableFuture<Void> deleteAll();
}
